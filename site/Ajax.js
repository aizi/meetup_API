/*-----------------------------------
http://localhost/site/index.html
------------------------------------*/

// <button class="delBtn"type="button" id="button-delete" data-id="${element['id']}" name="delete">supprimer</button>


window.onload = function () {
    // Récupération des meetups
    $.ajax({
        url: "http://localhost/meetup_API/meet-up/meetups",
    }).done(function (msg) {
        json = JSON.parse(msg);
        json.forEach(function (element) {
            $('.meetup').append(`
                <div class="slide active">
                    <div class="center" data-id="${element['id']}">
                    <div class="img" style="background-image:url(${element['image']});">
                    <div class="infos">
                            <a  href="#" onclick="getinfos(${element['id']})"><img src="infos.png" id="infos"></a>
                        </div>
                            </div>
                            <div class="ti">
                                <h2 class="h1 h2">${element['title']}</h2></br>
                            </div>

                            <div class="da">
                                <p class="h1d h2"><span class="span">Date du meetup :</span>${element['date']}</p></br>
                            </div>
                            <p class="subscriberCount"></p>
                            <div class="des">
                                <p class="h1des h2"><span class="span">Sujet :</span>${element['description']}</p></br>
                            </div>
                            <div class="butt-part-edit">
                                <button onclick="getMeetup(${element['id']})" class="edit-meet" >Modifier le meetup</button></br>
                                <button onclick="document.getElementById('id07').style.display='block'" class="edit-meet">Participez</button>
                                <button onclick="deleteMeetup(${element['id']})" class="edit-meet">Supprimer le Meetup</button>
                            </div>
                    </div>
                </div>
            `);
        })
    });

    // Récupération des locations
    $.ajax({
        url: "http://localhost/meetup_API/meet-up/Locations",
    }).done(function (msg) {
        json = JSON.parse(msg);
        json.forEach(function (element) {
            $('.location').append(`
                <div class="slideL activeL">
                <iframe width="385" height="250" frameborder="0" style="border:0"
                src="https://www.google.com/maps/embed/v1/place?q=${element['address']}${element['city']}${element['zip_code']}&key=AIzaSyD7mzQlP4N4n6PueKSzhtmMeZn97D8ppYs" allowfullscreen></iframe>
                <div class="des">
                    <p class="h1 h2"><span class="span">Sujet :</span>${element['description']}</p></br>
                </div>s
                </div>
            `);
        })
    });
//ajouter une location-------------------------------------------------------------------
var formAddMeetup = $('#add_meetup')

    formAddMeetup.submit(function () {
        event.preventDefault();
        var adresse= document.getElementById('adresse-meet').value;
        var city=document.getElementById('ville-meet').value;
        var zip_code=document.getElementById('zip-meet').value;
        $.ajax({
            method: "POST",
            url: "http://localhost/meetup_API/meet-up/locations",
            data: {adresse: adresse,city: city,zip_code: zip_code }
        }).done(function (msg) {

        })
    });
    //-----------------------------------------------------------------------

    // Récupération des speakers
    $.ajax({
        url: "http://localhost/meetup_API/meet-up/Speakers",
    }).done(function (msg) {
        json = JSON.parse(msg);
        json.forEach(function (element) {
            $('.speaker').append(`
                <div class="slideS activeS">
                <div class="ti">
                    <p class="h1 h2"><span class="span">Nom :</span>${element['first_name']}</p></br>
                </div>
                <div class="da">
                    <p class="h1 h2"><span class="span">Prenom :</span>${element['last_name']}</p></br>
                </div>
                <div class="des">
                    <p class="h1 h2"><span class="span">Sujet :</span>${element['description']}</p></br>
                </div>

                </div>
            `);
        })
    });

    //----------------------------------------------------------------------------------------
    // Récupération des subscribers
    $.ajax({
        url: "http://localhost/meetup_API/meet-up/Subscribers",
    }).done(function (msg) {
        json = JSON.parse(msg);
        json.forEach(function (element) {
            $('.subscriber').append(`
                <div class="slideS2 activeS2">
                <div class="ti">
                    <p class="h1 h2"><span class="span">Nom :</span>${element['first_name']}</p></br>
                </div>
                <div class="da">
                    <p class="h1 h2"><span class="span">Prenom :</span>${element['last_name']}</p></br>
                </div>
                <div class="des">
                    <p class="h1 h2"><span class="span">Email :</span>${element['mail_addr']}</p></br>
                </div>
                <div class="da">
                    <p class="h1 h2"><span class="span">Date d'inscription  :</span>${element['date_subscription']}</p></br>
                </div>
                </div>
            `);
        })
    });
    //---------------------------SUBSCRIBERS-------------------participez au meet up--------------------------
    var souscription = $('#souscription-form')
    souscription.submit(function (event) {
        event.preventDefault();
        var first_name = document.getElementById('fname_subscriber').value;
        var last_name = document.getElementById('lname_subscriber').value;
        var mail_addr = document.getElementById('mail_subscriber').value;
        var date_subscription = document.getElementById('date_subscriber').value;

        $.ajax({
            method: 'POST',
            url: "http://localhost/meetup_API/meet-up/Subscriber",
            data: { first_name: first_name, last_name: last_name, date_subscription: date_subscription, mail_addr: mail_addr }
        }).done(function (msg) {
            console.log(data)
            //$(`[data-id=${id}`).add();// avoir
        });
    })
    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!NOMBRE DE PARTICIPANTS!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
    $.ajax({
        url: "http://localhost/meetup_API/meet-up/Subscriber",
    }).done(function (msg) {
        json = JSON.parse(msg);
        $('.subscriberCount').append(`
                <h3>Nombres de participants:${json[0].participants}</h3>
        `);
    });
    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!pop up infos!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    // $.ajax({
    //     url: "http://localhost/meetup_API/meet-up/infos",
    // }).done(function (msg) {
    //     json = JSON.parse(msg);
    //     json.forEach(function (element) {
    //         $('.modal8').append(`

    //         <div class="container">
    //             <div class="des">
    //                 <p class="h1 h2"><span class="span">Sujet :</span>${element['1']}</p></br>
    //             </div>
    //             <div class="iframe"><iframe width="350" margin="30px"  "height="250" frameborder="0" style="border:0"
    //             src="https://www.google.com/maps/embed/v1/place?q=${element['address']}${element['city']}${element['zip_code']}&key=AIzaSyD7mzQlP4N4n6PueKSzhtmMeZn97D8ppYs" allowfullscreen></iframe></div>
    //         <h3><span>Date du meetup:</span>${element['2']}</h3>
    //             </div>
    //         `);
    //     })
    // });


    //---------------------------------------------add_meetup--------------------------------------------------------------------
    var formAddMeetup = $('#add_meetup')

    formAddMeetup.submit(function () {
        event.preventDefault();
        var title = document.getElementById('title-meetA').value;
        var description = document.getElementById('desc-meetA').value;
        var date = document.getElementById('date-meetA').value;
        var image = document.getElementById('image-meetA').value;
        var adresse= document.getElementById('adresse-meet').value;
        var city=document.getElementById('ville-meet').value;
        var zip=document.getElementById('zip-meet').value;
        $.ajax({
            method: "POST",
            url: "http://localhost/meetup_API/meet-up/meetups",
            data: { title: title, description: description, date: date, image: image,adresse: adresse,city: city,zip: zip }
        }).done(function (msg) {

        })
    });




    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    //------------------------------------login----------------------------------------------
    function connexionAccount() {
        var mailAddr = document.getElementById('mail_addrL').value;
        var password = document.getElementById('passwordL').value;

        console.log('envoi formulaire')
        $.ajax({
            url: 'http://localhost/meetup_API/meet-up/connexion',
            type: 'POST',
            data: { mailAddr: mailAddr, password: password }
        }).done(function (msg) {
            console.log(msg);
            json = JSON.parse(msg);
        });
    }

    $('#connexion').on('click', connexionAccount);
    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!  ADD-USER   INSCRIPTION!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    var formAdduser = $('#submitI')

    formAdduser.submit(function () {
        event.preventDefault();
        var firstName = document.getElementById('first_nameI').value;
        var lastName = document.getElementById('last_nameI').value;
        var mailAddr = document.getElementById('mail_addrI').value;
        var password = document.getElementById('passwordI').value;
        $.ajax({
            method: "POST",
            url: "http://localhost/meetup_API/meet-up/Inscription",
            data: { firstName: firstName, lastName: lastName, mailAddr: mailAddr, password: password }
        }).done(function (msg) {
            alert('OK');
        }).fail(function () {
            alert('FAUX');
        })
    })
}
//------------------------------------profile-------------------------------------------
// $.ajax({
//     url: "http://localhost/meetup_API/meet-up/profil"
// }).done(function (msg) {
//     console.log(msg);
//     json = JSON.parse(msg);
//     console.log(json);
//     json.forEach(function (element) {
//         $('.user').append(
//             '<h3>' + element['first_name'] + '</h3>' +
//             '<h3>' + element['last_name'] + '</h3>' +
//             '<h3>' + element['mail_addr'] + '</h3>' +
//             '<h3>' + element['date_subscription'] + '</h3>')

//     })
// });



    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!get les infos pour edité!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    //---------------------------------------------
    var formUpdateMeetup = $('#enregistrer');
    formUpdateMeetup.click(function (event) {
        event.preventDefault();
        console.log('updateForm')
        document.getElementById('id05').style.display = 'block';
        var title = document.getElementById('title-meetE').value;
        var description = document.getElementById('desc-meetE').value;
        var date = document.getElementById('date-meetE').value;
        var image = document.getElementById('image-meetE').value;
        var id = document.getElementById('id-meetE').value;
        $.ajax({
            method: 'POST',
            url: "http://localhost/meetup_API/meet-up/meetup/" + id,
            data: { title: title, description: description, date: date, image: image },
            dataType: 'json'
        }).done(function (msg) {
            var $meetup = $(`[data-id=${id}]`)
            var $titre = $meetup.find('.h1');
            var $description = $meetup.find('.h1des');
            var $date = $meetup.find('.h1d');
            // var $image = $meetup.find('.img');

            $titre.html(msg.title)
            $description.html(msg.description)
            $date.html(msg.date)
            $image.html(msg.image)
        });
    });


//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

function getMeetup(id) {
    document.getElementById('id05').style.display = 'block';
    $.ajax({
        url: "http://localhost/meetup_API/meet-up/meetup/" + id,
    }).done(function (msg) {
        json = JSON.parse(msg);
        console.log(json);
        $('#title-meetE').val(json.title);
        $('#desc-meetE').val(json.description);
        $('#date-meetE').val(json.date);
        $('#image-meetE').val(json.image);
        $('#id-meetE').val(json.id);


    });
}
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!ALL information!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
function getinfos(id) {
    document.getElementById('id08').style.display = 'block';
    $.ajax({
        // url: "http://localhost/meetup_API/meet-up/Allinfos",
        url: "http://localhost/meetup_API/meet-up/Allinfos/" + id,
    }).done(function (msg) {
        json = JSON.parse(msg);
        console.log(json);
        json.forEach(function (element) {
            $('.modal8').append(`
        <div class="container">
            <div class="des">
                <h2>${json[0].meetup_title}</h2>
                <p class="h1 h2"><span class="span">Sujet :</span>${json[0].meetup_description}</p></br>
            </div>
            <div class="iframe"><iframe width="350" margin="30px"  "height="250" frameborder="0" style="border:0"
            src="https://www.google.com/maps/embed/v1/place?q=${json[0].location_address}${json[0].location_city}&key=AIzaSyD7mzQlP4N4n6PueKSzhtmMeZn97D8ppYs" allowfullscreen></iframe></div>
            <h3><span>Date du meetup:${json[0].meetup_date}</span></h3>
            </div>
        `);
        })
    });
}
//-------------------------------------------------------------deleate meetup----------------------------------------------------

function deleteMeetup(id) {
    $.ajax({
        url: "http://localhost/meetup_API/meet-up/meetup/" + id,
        method: 'DELETE'
    }).done(function (msg) {//DELETE c'est la methode c tt commme POST
        $(`[data-id=${id}]`).remove()
    })

}










//----------------------------------------popup---------------
// Get the modal
var modal = document.getElementById('id01');
var modal2 = document.getElementById('id02');
var modal3 = document.getElementById('id03');
var modal5 = document.getElementById('id05');
var modal7 = document.getElementById('id07');
var modal8 = document.getElementById('id08');






// When the user clicks anywhere outside of the modal, close it
window.onclick = function (event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
    else
        if (event.target == modal2) {
            modal2.style.display = "none";
        }
        else
            if (event.target == modal3) {
                modal3.style.display = "none";
            }
            else
                if (event.target == modal5) {
                    modal5.style.display = "none";
                }
                else
                    if (event.target == modal7) {
                        modal7.style.display = "none";
                    }
                    else
                        if (event.target == modal8) {
                            modal8.style.display = "none";
                        }

}
//----------------------------------SLIDE-----------------------------------------------------
$('.right')
    .on('click', function () {

        $('.slide')
            .siblings('.active:not(:last-of-type)')
            .removeClass('active')
            .next()
            .addClass('active');
    });

$('.left')
    .on('click', function () {
        $('.slide')
            .siblings('.active:not(:first-of-type)')
            .removeClass('active')
            .prev()
            .addClass('active');
    });

//--------------------------------------------LOCATION
$('.rightL')
    .on('click', function () {

        $('.slideL')
            .siblings('.activeL:not(:last-of-type)')
            .removeClass('activeL')
            .next()
            .addClass('activeL');
    });

$('.leftL')
    .on('click', function () {
        $('.slideL')
            .siblings('.activeL:not(:first-of-type)')
            .removeClass('activeL')
            .prev()
            .addClass('activeL');
    });
//-----------------------------------------------------SPEAKERS
$('.rightS')
    .on('click', function () {

        $('.slideS')
            .siblings('.activeS:not(:last-of-type)')
            .removeClass('activeS')
            .next()
            .addClass('activeS');
    });

$('.leftS')
    .on('click', function () {
        $('.slideS')
            .siblings('.activeS:not(:first-of-type)')
            .removeClass('activeS')
            .prev()
            .addClass('activeS');
    });
//-----------------------------------------------subscribers
$('.rightS2')
    .on('click', function () {

        $('.slideS2')
            .siblings('.activeS2:not(:last-of-type)')
            .removeClass('activeS2')
            .next()
            .addClass('activeS2');
    });

$('.leftS2')
    .on('click', function () {
        $('.slideS2')
            .siblings('.activeS2:not(:first-of-type)')
            .removeClass('activeS2')
            .prev()
            .addClass('activeS2');
    });
//pop up infos





