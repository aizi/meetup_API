<?php
// include('config.php');
class Subscriber {
    // private $db;
    private $firstName;
    private $lastName;
    private $mailAddr;
    private $dateSubscription;

    public function getFirstName() {
        return $this->firstName;
    }

    public function setFirstName($firstName) {
        $this->firstName = $firstName;
    }

    public function getLastName() {
        return $this->lastName;
    }

    public function setLastName($lastName) {
        $this->lastName = $lastName;
    }

    public function getMailAddr() {
        return $this->mailAddr;
    }

    public function setMailAddr($mailAddr) {
        $this->mailAddr = $mailAddr;
    }

    public function getDateSubscription() {
        return $this->dateSubscription;
    }

    public function setDateSubscription($dateSubscription) {
        $this->dateSubscription = $dateSubscription;
    }

    public function getAll() {
        // Ici votre requête SQL (peut-être qu'un extends serait sympa pour la connexion en DB...)
        // Les données ci-dessous son fakes !
    $bdd = getConnection();
        $reponse = $bdd->query('SELECT * FROM subscriber');
        $subscriber = $reponse->fetchAll();
        return $subscriber;
    }



    function addsubscriber($first_name, $last_name, $date_subscription,$mail_addr){
        $bdd = getConnection();
        $req = $bdd->prepare('INSERT INTO subscriber (first_name,last_name,date_subscription,mail_addr) VALUES(:first_name,:last_name,:date_subscription,:mail_addr)');
        $req->execute(array(
        'first_name' => $first_name,
        'last_name'=>$last_name,
        'date_subscription'=> $date_subscription,
        'mail_addr'=> $mail_addr));
    }
    function getCount(){
        $bdd = getConnection();
        $count = $bdd->query('SELECT COUNT(*) AS participants FROM subscriber');
        $reponse = $count->fetchAll();
        return $reponse;
    }
}
