<?php
// include('config.php');
class Speaker {
    // private $db;
    private $firstName;
    private $lastName;
    private $description;

    public function getFirstName() {
        return $this->firstName;
    }

    public function setFirstName($firstName) {
        $this->firstName = $firstName;
    }

    public function getLastName() {
        return $this->lastName;
    }

    public function setLastName($lastName) {
        $this->lastName = $lastName;
    }
    public function getDescription() {
        return $this->description;
    }

    public function setDescription($description) {
        $this->description = $description;
    }


    public function getAll() {
        // Ici votre requête SQL (peut-être qu'un extends serait sympa pour la connexion en DB...)
        // Les données ci-dessous son fakes !
        $bdd = getConnection();
        $reponse = $bdd->query('SELECT * FROM speaker');
        $speaker = $reponse->fetchAll();
        return $speaker;
    }
    
}