<?php
// include('config.php');
class Location{
    // private $db;
    private $address;
    private $city;
    private $description;
    private $zip_code;


    public function getAddress() {
        return $this->address;
    }

    public function setAddress($address) {
        $this->address = $address;
    }

    

    public function getCity() {
        return $this->city;
    }

    public function setCity($city) {
        $this->city = $city;
    }

    public function getDescription() {
        return $this->description;
    }

    public function setDescription($description) {
        $this->description = $description;
    }

    public function getZip_code() {
        return $this->zip_code;
    }

    public function setZip_code($zip_code) {
        $this->zip_code = $zip_code;
    }


//     //Create
//     static function add($text){
//         $bdd = getConnection();
//         $req = $bdd->prepare('INSERT INTO location (afaire) VALUES (:afaire)');
//         $req->execute(array('afaire' => $text));
//     }
// //Delete
//     static function delete($id){
//         $bdd = getConnection();
//         $req = $bdd->prepare('DELETE FROM todo WHERE id=:id');
//         $req->execute(['id'=>$id]);

//     }
// //Update
//     static function update($id,$text, $done){
//         $bdd = getConnection();
//         $done = (int) $done;
//         $req = $bdd->prepare('UPDATE todo SET afaire=:afaire, done=:done WHERE id=:id');
//         $req->execute(array('afaire' => $text,'done'=>  $done,'id' => $id));
//     }




    public function getAll(){
        $bdd = getConnection();
        $reponse = $bdd->query('SELECT * FROM location');
        $location = $reponse->fetchAll();
        return $location;
    }

    function addlocation($adresse, $city, $zip_code){
        $bdd = getConnection();
        $req = $bdd->prepare('INSERT INTO location (address,city,zip_code) VALUES(:adresse,:city,:zip_code)');
        $req->execute(array(
        'adresse' => $adresse,
        'city'=>$city,
        'zip_code'=> $zip_code));
    }
}

