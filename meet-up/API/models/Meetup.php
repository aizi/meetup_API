<?php
 include('config.php');
class Meetup{
    private $db;
    private $title;
    private $description;
    private $date;
    private $image;


    public function getTitle() {
        return $this->title;
    }

    public function setTitle($title) {
        $this->title = $title;
    }

    public function getDescription() {
        return $this->description;
    }

    public function setDescription($description) {
        $this->description = $description;
    }

    public function getDate() {
        return $this->date;
    }

    public function setDate($date) {
        $this->date = $date;
    }

    public function getAll(){
        $bdd = getConnection();
        $reponse = $bdd->query('SELECT * FROM meetup');
        $meetup = $reponse->fetchAll();
        return $meetup;
    }

   

    function addmeetup($title, $description, $date,$image){
        $bdd = getConnection();
        $req = $bdd->prepare('INSERT INTO meetup (location_id,title,description,date,image) VALUES((SELECT MAX(id) FROM location),:title,:description,:date,:image)');
        $req->execute(array(
        'title' => $title,
        'description'=>$description,
        'date'=> $date,
        'image'=> $image));
    }

    function delete($id){
        $bdd = getConnection();
        $meetup = $bdd->prepare('DELETE FROM meetup WHERE id = :id');
        $meetup->execute(['id'=>$id]);
    }

    function update($id,$title, $description,$date,$image){
        $bdd = getConnection();
        $req = $bdd->prepare('UPDATE meetup SET title=:title, description=:description,date=:date,image=:image WHERE id=:id');
        $req->execute(array('title' => $title,'description'=>$description,'date'=>$date,'image'=>$image,'id' => $id));
    }
    public function getById($id){
        $bdd = getConnection();
        $reponse = $bdd->prepare('SELECT * FROM meetup WHERE id = :id');
        $reponse->execute(['id'=>$id]);
        $meetup = $reponse->fetch();
        return $meetup;
    }

    // function getAllinfos(){
    //     $bdd = getConnection();
    //     $reponse = $bdd->query('SELECT m.id as meetup_id,m.title as meetup_title,m.description as meetup_description,m.image as meetup_image,m.date as meetup_date,m.location_id as location_id,l.id as id_location,l.address as location_address,l.city as location_city FROM `meetup` as m INNER JOIN `location` as l ON m.location_id = l.id');
    //     $meetup = $reponse->fetchAll();
    //     return $meetup;
    // }
    function getAllinfos($id){
        $bdd = getConnection();
        $reponse = $bdd->query('SELECT m.id as meetup_id,m.title as meetup_title,m.description as meetup_description,m.image as meetup_image,m.date as meetup_date,m.location_id as location_id,l.id as id_location,l.address as location_address,l.city as location_city FROM `meetup` as m LEFT JOIN `location` as l ON m.location_id = l.id
        WHERE m.id ='.$id);
        $meetup = $reponse->fetchAll(\PDO::FETCH_ASSOC);
        return $meetup;
    }
}