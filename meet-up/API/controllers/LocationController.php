<?php

require_once 'API/controllers/Controller.php';
require_once 'API/models/Location.php';
require_once 'MeetupController.php';

class LocationController {
    function getAll() {
        $location = new Location();
        return json_encode($location->getAll());
    }

        function addlocation() {
            $meetup = new MeetupController();
            $adresse = $_POST['adresse'];
            $city = $_POST['city'];
            $zip_code = $_POST['zip_code'];
            $location = new Location();
            $location->addlocation($adresse, $city, $zip_code);
            $meetup->addmeetup();
    
        }
}