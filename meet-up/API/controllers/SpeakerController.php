<?php

require_once 'API/controllers/Controller.php';
require_once 'API/models/Speaker.php';

class SpeakerController {
    function getAll() {
        $speaker = new Speaker();
        return json_encode($speaker->getAll());
    }
}