<?php
require_once 'API/controllers/Controller.php';

require_once 'API/models/Meetup.php';
class MeetupController {

    function getAll() {
        session_start();
        $meetup = new Meetup();
        return json_encode($meetup->getAll());
    }

    function addmeetup() {
        $title = $_POST['title'];
        $description = $_POST['description'];
        $date = $_POST['date'];
        $image = $_POST['image'];
        $meetup = new Meetup();
        return  json_encode($meetup->addmeetup($title, $description, $date,$image));

    }
   
    function delete($id){
        $meetup = new Meetup();
        $meetup->delete($id);
        // header('Location:'.$_SERVER['HHTP_REFERER']);
    }
    function update($id){
        $title = $_POST['title'];
        $description = $_POST['description'];
        $date = $_POST['date'];
        $image = $_POST['image'];
        $meetup = new Meetup();
        $meetup->update($id,$title, $description, $date,$image);
        return json_encode(compact('title', 'id', 'description', 'image', 'date'));
     }
     function getById($id) {
        $meetup = new Meetup();
        return  json_encode($meetup->getById($id));
    }
    // function getAllinfos() {
    //     $meetup = new Meetup();
    //     return json_encode($meetup->getAllinfos());
    // }
    function getAllinfos($id) {
        $meetup = new Meetup();
        return json_encode($meetup->getAllinfos($id));
    }
    

}