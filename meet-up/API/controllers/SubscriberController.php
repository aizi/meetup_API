<?php

require_once 'API/controllers/Controller.php';
require_once 'API/models/Subscriber.php';

class SubscriberController {
    function getAll() {
        $subscriber = new Subscriber();
        return  json_encode($subscriber->getAll());
    }
// function getWithId($id){
//     echo json_encode('coucou'.$id);
// }
    function addsubscriber() {
        $first_name = $_POST['first_name'];
        $last_name = $_POST['last_name'];
        $date_subscription = $_POST['date_subscription'];
        $mail_addr = $_POST['mail_addr'];
        $subscriber = new Subscriber();
        return  json_encode($subscriber->addsubscriber($first_name, $last_name, $date_subscription,$mail_addr));
        // header('Location:'.$_SERVER['HHTP_REFERER']);

    }
    function getCount() {
        $subscriber = new Subscriber();
        return  json_encode($subscriber->getCount());
    }
   
}
