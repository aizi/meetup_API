<?php

require_once 'API/controllers/Controller.php';

class DefaultController extends Controller {
    function defaultAction() {
        $data = array('Message' => 'Welcome on my first API');
        return json_encode($data);
    }

}