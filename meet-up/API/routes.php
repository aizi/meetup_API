<?php


use Pecee\SimpleRouter\SimpleRouter;
require_once 'controllers/DefaultController.php';
require_once 'controllers/MeetupController.php';
require_once 'controllers/SubscriberController.php';
require_once 'controllers/SpeakerController.php';
require_once 'controllers/LocationController.php';
require_once 'controllers/LoginController.php';



// on ajoute un préfixe car le site se trouve dans mon cas à l'adresse :
// http://localhost/simplon/routeur/
$prefix = '/meetup_API/meet-up';

SimpleRouter::group(['prefix' => $prefix], function () {
    SimpleRouter::get('/', 'DefaultController@defaultAction');
    SimpleRouter::get('/meetups', 'MeetupController@getAll'); // Tous
    SimpleRouter::get('/meetup/{id}', 'MeetupController@getById'); // Un seul
    SimpleRouter::delete('/meetup/{id}', 'MeetupController@delete'); // Supprimer

    SimpleRouter::post('/meetup/{id}', 'MeetupController@update'); // Mettre à jour

    SimpleRouter::post('/meetups', 'MeetupController@addmeetup');
    SimpleRouter::post('/locations', 'LocationController@addlocation');


   // SimpleRouter::get('/Subscriber/{id}', 'SubscriberController@getWithId');//a voir
   
   SimpleRouter::get('/Subscriber', 'SubscriberController@getCount');

    SimpleRouter::post('/Subscriber', 'SubscriberController@addsubscriber');
    SimpleRouter::get('/Subscribers', 'SubscriberController@getAll');
    SimpleRouter::get('/Speakers', 'SpeakerController@getAll');
    SimpleRouter::get('/Locations', 'LocationController@getAll');
    SimpleRouter::get('/infos', 'LocationController@getAll');
    
    // SimpleRouter::get('/Allinfos', 'MeetupController@getAllinfos');
    SimpleRouter::get('/Allinfos/{id}', 'MeetupController@getAllinfos');

    // SimpleRouter::get('/login', 'LoginController@selectUser');
    SimpleRouter::post('/inscription', 'LoginController@Inscription');

    // 
    SimpleRouter::post('/connexion', 'LoginController@login');



});